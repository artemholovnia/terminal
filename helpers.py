from collections import namedtuple
Parameter = namedtuple('Parameter', ('int_repr', 'str_repr', 'data_type', 'required'))
FormField = namedtuple('FormField', 
                       ('initial', 'errors', 'label', 'required'),
                       defaults=(None, [], None, False))
Form = namedtuple('Form', 
                  ('fields', 'errors'), 
                  defaults=([], []))
TemplateMessage = namedtuple('TemplateMessage', 
                             ('body', 'level'),
                             defaults=('', 'info')
                             )
LEVEL_TO_BOOSTRAP_MAP = {
    'info': 'success',
    'error': 'danger'
}

