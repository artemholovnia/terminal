----

## Requirements
Requires Python 3.6 or newer

## Installation


```
pip install -r requirements.txt
```

Change DATABASE variable in settings.py file

```
DATABASE = {
    'DB_USER':'terminal',
    'DB_PASSWORD':'terminal',
    'DB_NAME':'terminal',
    'DB_HOST':'localhost',
    'DB_PORT':3306
}
```

Start application

```
python main.py
```
