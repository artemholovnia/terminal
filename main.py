import os

from aiohttp import web
import aiohttp_jinja2
import jinja2

from clients import db, views
import settings as conf


def init_app():
    app = web.Application()
    app.router.add_get('/', views.CreateClient, name='create')
    app.router.add_post('/', views.CreateClient)
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(searchpath=os.path.join(conf.BASE_DIR, 'templates')))
    db.init_db(app)
    return app
   
def main():
    app = init_app()
    web.run_app(app)
   
if __name__ == '__main__':
    main()
   
