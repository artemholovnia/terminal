import asyncio

from aiohttp import web
import aiohttp_jinja2

from clients import db
import helpers
import settings


CREATE_CLIENT_DELAY = 0
POST_DELAY = 10

class CreateClient(web.View):
    def common_context(self):
        form = helpers.Form([])
        for field in settings.ALLOWED_PARAMETERS:
            form_field = helpers.FormField(label=field.str_repr.lower(), required=field.required)
            form.fields.append(form_field)
        return {'form':form}
    
    @classmethod
    async def _create_client(cls, request):
        await asyncio.sleep(CREATE_CLIENT_DELAY)
        data = await request.post()
        clean_data = {}
        allowed_fields = [i.str_repr.lower() for i in settings.ALLOWED_PARAMETERS]
        required_fields = [i.str_repr.lower() for i in filter(lambda x: x.required, settings.ALLOWED_PARAMETERS)]
        for k, v in data.items():
            if k in allowed_fields and v:
                clean_data[k] = v
        if clean_data and all(map(lambda x: x in clean_data.keys(), required_fields)) :
            db.create_client(request.app['db_conn'], clean_data)
            valid = True
        else:
            valid = False
        return clean_data, valid
      
    async def get(self):
        context = self.common_context()
        response = aiohttp_jinja2.render_template('clients/create.html', self.request, context)
        return response
  
    async def post(self):
        future = asyncio.ensure_future(self._create_client(self.request))
        await asyncio.sleep(POST_DELAY)
        context = self.common_context()
        if not future.done():
            future.cancel()
            context['message'] = helpers.TemplateMessage('Client can\'t be created', helpers.LEVEL_TO_BOOSTRAP_MAP.get('error'))
        else:
            client, valid = future.result()
            if not valid:
                context['message'] = helpers.TemplateMessage(f'Form data is invalid', helpers.LEVEL_TO_BOOSTRAP_MAP.get('error'))
            else:
                context['message'] = helpers.TemplateMessage(f'Client \'{client["name"]}\' was created', helpers.LEVEL_TO_BOOSTRAP_MAP.get('info'))
        return aiohttp_jinja2.render_template('clients/create.html', self.request, context)
