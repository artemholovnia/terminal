from sqlalchemy.engine import create_engine
from sqlalchemy.sql.expression import select
from sqlalchemy.sql.schema import MetaData, Table, Column, ForeignKey, \
    UniqueConstraint
from sqlalchemy.sql.sqltypes import Integer, String

import settings
from sqlalchemy.sql import and_


metadata = MetaData()

client = Table(
    'clients__clients', metadata,

    Column('id', Integer, primary_key=True),
#     Column('session', String(256), unique=True)
)

parameter_type = Table(
    'clients__parameters_types', metadata,
    Column('id', Integer, primary_key=True),
    Column('int_repr', Integer, unique=True),
    Column('str_repr', String(64), unique=True)
)
 
client_parameter = Table(
    'clients__clients_parameters', metadata,
    Column('id', Integer, primary_key=True),
    Column('client_id', ForeignKey('clients__clients.id', ondelete='CASCADE')),
    Column('parameter_type_id', ForeignKey('clients__parameters_types.id', ondelete='CASCADE')),
    Column('value', String(128)),
     
    #===========================================================================
    # Unique together in client_id and info_type_id columns
    #===========================================================================
    UniqueConstraint('client_id', 'parameter_type_id')
)

def init_db(app):
    db_conf = settings.DATABASE
    dsn = f'mysql://{db_conf["DB_USER"]}:{db_conf["DB_PASSWORD"]}@{db_conf["DB_HOST"]}:{db_conf["DB_PORT"]}/{db_conf["DB_NAME"]}'
    engine = create_engine(dsn)
    conn = engine.connect()
    app['db_conn'] = conn
    metadata.create_all(engine, tables=metadata.sorted_tables)
    for s in init_parameters(app):
        conn.execute(s)
    init_parameters(app)
    return engine

def init_parameters(app):
    conn = app['db_conn']
    for p in settings.ALLOWED_PARAMETERS:
        sql = select([parameter_type.c.int_repr, parameter_type.c.str_repr]).\
        where(
            and_(parameter_type.c.int_repr==p.int_repr,
                 parameter_type.c.str_repr==p.str_repr)
        )
        if not conn.execute(sql).rowcount > 0:
            yield parameter_type.insert().values(int_repr=p.int_repr, str_repr=p.str_repr)

def create_client(conn, data):
    client_id = conn.execute(client.insert()).lastrowid
    parameters = {str_repr.lower(): id for id, str_repr in conn.execute(select([parameter_type.c.id, parameter_type.c.str_repr]))}
    columns = [client_parameter.c.client_id, client_parameter.c.parameter_type_id, client_parameter.c.value]
    processed = {}
    for p, v in data.items():
        values = [client_id, parameters[p], v]
        values = {'client_id': client_id,
                  'parameter_type_id': parameters[p],
                  'value': v}
        try:
            conn.execute(client_parameter.insert(columns).values(**values))
            processed[p] = v
        except:
            pass
    return processed